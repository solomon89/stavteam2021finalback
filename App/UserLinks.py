from flask import  request
from flask_restx import Resource
from App.core import postgresql


class UserLinks(Resource):
    def get(self, UserId):
        Sql = """
            select fio,links."Name", links.id "LinkId", linktype."Name" "linktype" from 
            users us inner join
            userlink on us.id = "userlink".iduser 
            inner join 
            links on "links".id = "userlink".idlink 
            inner join 
            linktype on "linktype".id = "links".type_id 
            where us.id = """+str(UserId)
        dataFromSQL = postgresql.execute(Sql)
        data = []

        for row in dataFromSQL:
            userLinkModel = UserLinkModel(row[0],row[1],row[2],row[3])
            data.append(userLinkModel.serialize())
        print(data) 
        return data

class UserLinkModel:
    def __init__(self,fio, linksName, LinkId, linktype):
        self.fio = fio
        self.linksName = linksName
        self.LinkId = LinkId
        self.linktype = linktype
    def serialize(self):
        return {"fio": self.fio,
                "linksName": self.linksName,
                "linktype": self.linktype,
                "LinkId": self.LinkId}
