import tempfile
import subprocess
from lxml import etree
import os
gptPath = r'D:\snap\bin\gpt.exe'


def runCommand(commandName, sourceFileName, xml):
    tmpFileName = tempfile.mkstemp()[1]+'.xml'
    xml.write(tmpFileName)
    res = subprocess.run([gptPath, tmpFileName])
    return res


def splitImage(inputFile, polarisations, subswath):
    """S-1 TOPS Split is applied to the data to select only those bursts which are required for the analysis."""
    command = 'TOPSAR-Split'
    #baseXML = etree.parse('App/imageProcessingGraphs/splitGraph.xml')
    baseXML = etree.parse(os.getcwd()+r'\imageProcessingGraphs\splitGraph.xml')
    root = baseXML.getroot()
    res = None
    if inputFile is not None:
        for data in root.findall("./node/sources/source"):
            data.set('source', inputFile)
        if polarisations is not None:
            root.set('selectedPolarisations', polarisations)
            #params = params + ' --PselectedPolarisations=' + polarisations
        if subswath is not None:
            root.set('subswath', subswath)
            #params = params + ' --Psubswath=' + subswath
        res = runCommand(command, inputFile, baseXML)
    return res

res = splitImage(r'C:\Kazan\S1A_IW_SLC__1SDV_20211120T030444_20211120T030514_040649_04D28F_370D.zip', 'VV', 'IW2')

