from flask import request
from flask_restx import Resource
from App.core import postgresql

Routes = {1 : [5,6,7,8]}

class Route(Resource):
    def get(self, route_id):
        sql = '''Select ST_AsGeoJSON(ST_SetSRID(public.iso.geom, 3857))
                    from public.iso
                    where ST_Intersects(public.iso.geom,(select ST_Buffer(el.geom::geography,el.line_type::int) 
                                                         from public.electro_links el
                                                            where el.id='''+str(route_id)+'''))
                                                            and public.iso.elev>=4 and public.iso.elev<=150 limit 50'''
        dataFromSQL = postgresql.execute(sql)
        data = []
        for row in dataFromSQL:
            data.append({'isoline' : row[0]})
        return data

    def post(self, route_id):
        Routes[route_id] = request.form['data']
        return {route_id: Routes[route_id]}

    def delete(self, route_id):
        Routes[route_id] = request.form['data']
        return {route_id: Routes[route_id]}