from flask import request
from flask_restx import Resource
from App.core import postgresql


class ElectroLinks(Resource):
    def get(self, UserId):
        if UserId == 1:
            params = ' (785,786,1509,1506) '
        Sql = """
            SELECT id, ST_AsGeoJSON(ST_SetSRID(geom,3857)),  name, voltage,
                   ST_AsGeoJSON(ST_SetSRID(ST_Buffer(geom::geography,line_type::int),3857)) bufferZone FROM public.electro_links
            where id in  """ + params
        dataFromSQL = postgresql.execute(Sql)
        data = []

        for row in dataFromSQL:
            ElectroLinkModels = ElectroLinkModel(row[0], row[1], row[2], row[3], row[4])
            data.append(ElectroLinkModels.serialize())
        print(data)
        return data


class ElectroLinkModel:
    def __init__(self, id, geom, osm_type, voltage, bufferZone):
        self.id = id
        self.geom = geom
        self.osm_type = osm_type
        self.voltage = voltage
        self.bufferZone = bufferZone
    def serialize(self):
        return {"id": self.id,
                "geom": self.geom,
                "osm_type": self.osm_type,
                "voltage": self.voltage,
                "bufferZone": self.bufferZone
                }