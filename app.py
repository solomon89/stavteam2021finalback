from flask import Flask
from flask_restx import  Api,fields,Resource,reqparse
from flask_cors import CORS
from App import Routes
from App import UserLinks
from App import login
from App import ElectroLinks

app = Flask(__name__)
api = Api(app)
model = api.model('Model', {
    'task': fields.String,
    'uri': fields.Url('route_ep')
})
app.config['JSON_AS_ASCII'] = False
cors = CORS(app, resources={r"*": {"origins": "*"}})
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['CORS_HEADERS'] = 'Access-Control-Allow-Origin = *'

#api.add_resource(Routes.Route, '/<int:route_id>','', endpoint='route_ep')
api.add_resource(Routes.Route, '/<int:route_id>', endpoint='route_ep')
api.add_resource(UserLinks.UserLinks, '/UserLinks/<int:UserId>', endpoint='userroutes_ep')
api.add_resource(ElectroLinks.ElectroLinks, '/ElectroLinks/<int:UserId>', endpoint='electroroutes_ep')

app.config.SWAGGER_SUPPORTED_SUBMIT_METHODS = ["get", "post"]
app.config['BUNDLE_ERRORS'] = True

resource_fields = api.model('Resource', {
    'logins': fields.String,
    'password': fields.String,
})
parser = reqparse.RequestParser()
parser.add_argument('logins')
parser.add_argument('password')

@api.route('/Login/', endpoint='login')
class auth(Resource):
    @api.expect(resource_fields)
    #@api.marshal_with(resource_fields)
    def post(self):
        args = parser.parse_args()
        loginData = login.login(str(args["logins"]),str(args["password"]))  
        return loginData



if __name__ == '__main__':
    app.run(debug=True)

